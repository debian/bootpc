# Last updated : Tue Dec 15 03:38:58 1998
# Version 0.64
#
# $Revision: 1.9 $
#
PROG=bootpc
CFLAGS=-O2 -Wall

# list the Cfiles here
CFILES=bootpc.c main.c

# List the header files here
CHEADS=bootpc.h bootp.h bptypes.h log.h

# Other files we want to have arround in distrib
MISC=Makefile MANIFEST LICENCE NOTICE LSM DHCP README WHEN-IT-FAILS VERSION \
	 ChangeLog rc.bootp VERSION

# the delete command
RM=/bin/rm
SED=/usr/bin/sed

# we generate this from the list of C files above
OBJS=$(CFILES:.c=.o)

%.o:	%.c $(CHEADS)
	$(CC) $(CFLAGS) -c $(*).c

all:	$(PROG)

clean:
	${RM} -f ${OBJS} ${PROG} *~ *.tar *.tar.gz *.tgz

${PROG}: ${OBJS}
	${CC} -o ${PROG} ${CFLAGS} ${OBJS}
	strip ${PROG}

${PROG}-static: ${OBJS}
	${CC} -static -o ${PROG} ${CFLAGS} ${OBJS}
	strip ${PROG}

# Make up a distribution file from the bits we want people to see
distrib:  $(CFILES) $(CHEADS) $(MISC)
	set `cat VERSION` ;\
	rdate=`date '+%d%h%y'|tr '[a-z]' '[A-Z]'`; \
	distname=bootpc-$$1 ; \
	rm -fr $$distname $$distname.tar $$distname.tar.gz; \
	mkdir $$distname; \
	chmod 755 $$distname; \
	for i in `cat MANIFEST` ; do \
	  [ -f $$i ] && sed -e "s/0.64/$$1/g;s/17DEC98/$$rdate/g" $$i > $$distname/$$i && echo $$i; \
	done; \
	ln -s LSM $$distname/$$distname.lsm; \
	cp $$distname/$$distname.lsm .; \
	chmod -R a+rx $$distname; \
	tar -cvf $$distname.tar $$distname; \
	gzip -9v $$distname.tar; \
	rm -fr $$distname
