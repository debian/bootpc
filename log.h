#ifndef _H_LOG
#define _H_LOG

#include <stdio.h>

extern FILE * log;
extern int logfd;

extern int testing;
extern int hackDisk;

#define logMessage doLogMessage

void doLogMessage(char * s, ...);
void openLog(void);
void closeLog(void);

#endif
